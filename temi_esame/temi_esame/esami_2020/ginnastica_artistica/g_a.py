import operator


def read_athletes(filename):
    athletes = list()
    try:
        with open(filename, "r", encoding="utf-8") as file:
            for line in file:
                [name, surname, gender, nat, *scores] = line.rstrip().split(" ")
                for i in range(len(scores)):
                    scores[i] = float(scores[i])
                score = sum(sorted(scores)[1:4])
                athletes.append({
                    'name': name,
                    'surname': surname,
                    'gender': gender,
                    'nat': nat,
                    'score': score
                })
    except FileNotFoundError:
        print(f"File {filename} not found.")
        exit(-1)

    return athletes


def get_female_winner(athletes):
    winner = None
    max_score = -1
    for athlete in athletes:
        if athlete['gender'] == 'F':
            if athlete['score'] > max_score:
                winner = athlete
                max_score = athlete['score']
    return winner


def get_nation_ranking(athletes):
    ranking = []
    """
    1. Ciclo su tutti gli atleti
        2. Prendo la nazione, ed aggiorno un contatore dei punti -> dizionario
        dizionario?
        chiavi: nazioni 
        valori: punteggio
    """
    ranking_dict = {}
    for athlete in athletes:
        if athlete['nat'] in ranking_dict:
            # aggiornare il punteggio
            ranking_dict[athlete['nat']] += athlete['score']
        else:
            # aggiungere
            ranking_dict[athlete['nat']] = athlete['score']

    for key in ranking_dict.keys():
        ranking.append({'name': key, 'score': ranking_dict[key]})

    return sorted(ranking, key=operator.itemgetter('score'), reverse=True)[0:3]


def get_score(a):
    return a[1]


def get_nation_ranking_v2(athletes):
    """
    1. Ciclo su tutti gli atleti
        2. Prendo la nazione, ed aggiorno un contatore dei punti -> dizionario
        dizionario?
        chiavi: nazioni
        valori: punteggio
    """
    ranking_dict = {}
    for athlete in athletes:
        if athlete['nat'] in ranking_dict:
            # aggiornare il punteggio
            ranking_dict[athlete['nat']] += athlete['score']
        else:
            # aggiungere
            ranking_dict[athlete['nat']] = athlete['score']

    return sorted(ranking_dict.items(), key=get_score, reverse=True)[0:3]


def main():
    athletes = read_athletes("punteggi.txt")
    """
    Vincitrice femminile:
    Veronica Servente, ITA – Punteggio: 27.2
    """
    # 1. Female winner
    f_m = get_female_winner(athletes)
    if f_m is not None:
        print(f"Vincitrice femminile:\n{f_m['name']} {f_m['surname']}, {f_m['nat']} - Punteggio: {f_m['score']}")
    else:
        print(f"Non hanno partecipato atlete di sesso femminile")

    """
    Classifica complessiva nazioni:
    1°) ITA - Punteggio totale: 55.9
    2°) USA – Punteggio Finale: 52.2
    3°) GRB – Punteggio Finale: 50.4
    """

    # 2. Classifica nazioni
    nat_scores = get_nation_ranking(athletes)
    print(f"Classifica complessiva nazioni:")
    for i, nat in enumerate(nat_scores):
        print(f"{i + 1}°) {nat['name']} - Punteggio totale: {nat['score']}")

    nat_scores = get_nation_ranking_v2(athletes)
    print(f"Classifica complessiva nazioni:")
    for i, nat in enumerate(nat_scores):
        print(f"{i + 1}°) {nat[0]} - Punteggio totale: {nat[1]}")


if __name__ == "__main__":
    main()
