"""
Soluzione basata sul vostro codice. Ho cercato solo di riorganizzare il codice
in una maniera più efficiente. Non è completa, in quanto avrei dovuto fare troppe
modifiche e penso che sia meglio farle insieme.
"""
from operator import itemgetter


def leggi_file(filename):
    atleti = []
    try:
        with open(filename, 'r', encoding='utf-8') as file:
            for line in file:
                line = line.split()
                atleta = {
                    'nome': line[0],
                    'cognome': line[1],
                    'sesso': line[2],
                    'nazioni': line[3],
                    'punteggi': line[4:],
                }
                atleti.append(atleta)
            return atleti
    except FileNotFoundError:
        print('Attenzione! File non trovato!')
        exit(-1)


# Queste 2 funzioni vanno molto bene, sono parte di un buon paradigma di programmazione
# chiamato pipe.
def lista_F(a):
    lista_atlete = []
    for i in a:
        if i['sesso'] == 'F':
            lista_atlete.append(i)
    return lista_atlete


def lista_M(a):
    lista_atleti = []
    for i in a:
        if i['sesso'] == 'M':
            lista_atleti.append(i)
    return lista_atleti


# Il nome i questa funzione è un po' misleading, in qunato calcola solo il punteggio
def donna_miglior_punteggio(v):
    for i in v:
        lista = i['punteggi']
        lista_punteggi = []
        for a, b in enumerate(lista):
            lista_punteggi.append(float(b))
        somma = sum(lista_punteggi) - max(lista_punteggi) - min(lista_punteggi)
        i['punteggi'] = round(somma, 2)
    return v


def vincitrice(d):
    vincitrici = []
    d.sort(key=itemgetter('punteggi'))
    vincitrici.append(d[-1])
    for i in range(len(d) - 1):
        if d[i]['punteggi'] == d[-1]['punteggi']:
            vincitrici.append(d[i])
    return vincitrici


# Questa funzione è strana
def classifica(tot):
    usa = []
    grb = []
    rus = []
    ita = []
    for i in tot:
        if i['nazioni'].lower() == 'usa':
            usa.append(i['punteggi'])
        if i['nazioni'].lower() == 'grb':
            grb.append(i['punteggi'])
        if i['nazioni'].lower() == 'rus':
            rus.append(i['punteggi'])
        if i['nazioni'].lower() == 'ita':
            ita.append(i['punteggi'])

    dizionario = {
        'usa': sum(usa),
        'grb': sum(grb),
        'rus': sum(rus),
        'ita': sum(ita),
    }

    return dizionario


def main():
    # nome vincitrice femminile = somma di 5 punteggi - min - max
    atleti_tot = leggi_file('punteggi.txt')
    atlete = lista_F(atleti_tot)

    punti_atlete = donna_miglior_punteggio(atlete)
    win = vincitrice(punti_atlete)
    print('Vincitrice/i femminile: ')
    print(win)

    # classifica delle tre nazioni, sia atleti F che M, calcola somma di tutti atleti - min e max di ogni atleta
    atleti = lista_M(atleti_tot)
    punti_atleti = donna_miglior_punteggio(atleti)

    punti_tot = punti_atleti + punti_atlete
    print(classifica(punti_tot))


# memoria
if __name__ == '__main__':
    main()
