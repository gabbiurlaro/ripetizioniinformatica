"""
Tema d'esame hacking
Soluzione di Gabriele Iurlaro
"""


def read_products(filename):
    products = dict()
    try:
        with open(filename, "r", encoding="utf-8") as file:
            for line in file:
                [id_product, id_seller] = line.rstrip().split(" ")
                products[id_product] = id_seller
    except FileNotFoundError:
        print(f"File {filename} not found. Try again.")
        exit(-1)
    return products


def check_transactions(filename, products):
    suspects = dict()
    try:
        with open(filename, "r", encoding="utf-8")as file:
            for line in file:
                [id_product, id_seller] = line.rstrip().split(" ")
                if id_product in products:
                    if products[id_product] != id_seller:
                        # suspect transaction
                        if id_product in suspects:
                            suspects[id_product]['seller'].append(id_seller)
                        else:
                            suspects[id_product] = {
                                'seller': [id_seller]
                            }
                        suspects[id_product]['seller'].append(products[id_product])

    except FileNotFoundError:
        print(f"File {filename} not found. Try again.")
        exit(-1)
    return suspects


def main():
    products = read_products("prodotti.txt")
    suspects = check_transactions("acquisti.txt", products)

    if suspects:
        print(f"Elenco transazioni sospette")
        for product in suspects:
            print(f"Codice prodotto: {product}")
            print(f"Rivenditore ufficiale: {products[product]}")
            print(f"Lista rivenditori:", end="")
            for r in suspects[product]['seller']:
                print(f"{r}",end=" ")
            print("\n")


if __name__ == "__main__":
    main()
