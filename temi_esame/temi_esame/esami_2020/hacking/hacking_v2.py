def read_products():
    products = dict()
    try:
        file = open("prodotti.txt", "r", encoding="utf-8")
        for line in file:
            fields = line.rstrip().split(" ")
            id_product = fields[0]
            id_seller = fields[1]
            products[id_product] = id_seller
        file.close()
    except FileNotFoundError:
        print(f"FILE NOT FOUND")
        exit(-1)
    return products

def check_transactions(products):
    signalings = list()
    try:
        file = open("acquisti.txt", "r", encoding="utf-8")
        for line in file:
            fields = line.rstrip().split(" ")
            id_product = fields[0]
            id_seller = fields[1]
            true_seller = products[id_product]
            if id_seller != true_seller:
                # segnaliamo
                found = False
                for signal in signalings:
                    if signal['id_product'] == id_product:
                        found = True
                        # Dobbiamo aggiornare la segnalazione
                        signal['list'].append(id_seller)

                if not found:
                    # Quando il prodotto non è stato mai segnalato.
                    signal = {
                        'id_product': id_product,
                        'seller': true_seller,
                        'list': [true_seller, id_seller]
                    }
                    signalings.append(signal)

        file.close()
    except FileNotFoundError:
        print(f"FILE NOT FOUND")
        exit(-1)
    return signalings


def main():
    # 1. Legger il file dei prodotti
    products = read_products()
    print(products)
    # 2. leggere il file acquisti
    signalings = check_transactions(products)

    print(signalings)

if __name__ == "__main__":
    main()