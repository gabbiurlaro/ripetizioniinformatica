import operator


def lista_calciatori(filename):
    lista = []
    try:
        file = open(filename, 'r', encoding='utf-8')
        for riga in file:
            x = riga.rstrip().split(',')
            calciatore = {
                'cognome': x[0],
                'ruolo': x[2],
                'quotazione': int(x[3])
            }
            lista.append(calciatore)
        file.close()
        return lista
    except FileNotFoundError:
        print('il file non è stato trovato')
        exit(-1)

def acquista_portieri(c):
    n_p = 3
    budget_acquisto = 20
    lista = []
    quantita = 0
    for i, v in enumerate(c):
        da_spendere = budget_acquisto - n_p
        if v['ruolo'] == ' portiere' and quantita < 3:
            if da_spendere >= v['quotazione']:
                lista.append(v)
                n_p = n_p-1
                budget_acquisto = budget_acquisto - v['quotazione']
                c.pop(i)
                quantita = quantita+1
    return lista

def acquista_difensori(c):
    n_d = 8
    budget_acquisto = 40
    lista = []
    quantita = 0
    for i,v in enumerate(c):
        da_spendere = budget_acquisto - n_d
        if v['ruolo'] == ' difensore' and quantita < 8:
            if budget_acquisto >= v['quotazione']:
                lista.append(v)
                n_d = n_d-1
                buget_acquisto = budget_acquisto-v['quotazione'] - n_d
                c.pop(i)
                quantita = quantita+1
    return lista

def acquista_centrocampisti(c):
    n_c = 8
    buget_acquisto = 80-n_c
    lista = []
    quantita = 0
    for i,v in enumerate(c):
        if v['ruolo'] == ' centrocampista' and quantita < 8:
            if buget_acquisto >= v['quotazione']:
                lista.append(v)
                n_c = n_c-1
                buget_acquisto = buget_acquisto-v['quotazione'] - n_c
                c.pop(i)
                quantita = quantita+1
    return lista

def acquista_attaccanti(c):
    n_a = 6
    buget_acquisto = 120-n_a
    lista = []
    quantita = 0
    for i, v in enumerate(c):
        if v['ruolo'] == ' attacante' and quantita < 6:
            if buget_acquisto >= v['quotazione']:
                lista.append(v)
                n_a = n_a - 1
                buget_acquisto = buget_acquisto-v['quotazione'] - n_a
                c.pop(i)
                quantita = quantita+1
    return lista


def main():
    calciatori = lista_calciatori('fantacalcio.txt')
    calciatori.sort(reverse=True, key= operator.itemgetter('quotazione'))
    portieri = acquista_portieri(calciatori)
    print(len(portieri))
    difensori = acquista_difensori(calciatori)
    print(len(difensori))
    centrocampisti = acquista_centrocampisti(calciatori)
    print(len(centrocampisti))
    attaccanti= acquista_attaccanti(calciatori)
    print(len(attaccanti))

    print(f"portieri: ",end="")
    for i in portieri:
        print(f"{i['cognome']:2} {i['quotazione']:}", end='  ')
    print()
    print(f"difensori: ",end="")
    for i in difensori:
        print(f"{i['cognome']:2} {i['quotazione']:}", end='  ')
    print()
    print(f"centrocampisti: ",end="")
    for i in centrocampisti:
        print(f"{i['cognome']:2} {i['quotazione']:}", end='  ')
    print()
    print(f"attacanti: ",end="")
    for i in attaccanti:
        print(f"{i['cognome']:2} {i['quotazione']:}", end='  ')


if __name__ == '__main__':
    main()