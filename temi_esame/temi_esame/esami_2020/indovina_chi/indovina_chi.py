from copy import deepcopy


def read_people(filename):
    people = list()
    try:
        with open(filename, "r", encoding="utf-8") as file:
            for line in file:
                if "Nome" in line:
                    # prima riga, per ora la escludiamo
                    pass
                else:
                    fields = line.rstrip().split(";")
                    person = {
                        'Nome': fields[0],
                        'Sesso': fields[1],
                        'Colore Capelli': fields[2],
                        'Lunghezza Capelli': fields[3],
                        'Occhiali': fields[4],
                        'Cappello': fields[5],
                        'Baffi': fields[6],
                        'Barba': fields[7],
                        'Pelato': fields[8],
                    }
                    people.append(person)
    except FileNotFoundError:
        print(f"{filename} not found")
        exit(-1)
    return people


def indovina(people, filename):
    remaining = deepcopy(people)
    try:
        with open(filename, "r", encoding="utf-8") as file:
            count = 1
            for line in file:
                [caratteristica, valore] = line.rstrip().split("=")
                # fields = line.rstrip().split("=")
                # caratteristica = fields[0]
                # valore = fields[1]
                real_list = deepcopy(remaining)
                for index, person in enumerate(remaining):
                    # se la persona non ha la caratteristica, la eliminiamo. con pop
                    if person[caratteristica] != valore:
                        # la elimino
                        real_list.remove(person)
                remaining = real_list

                print(f"Mossa {count} - rimaste {len(remaining)} persone")
                for person in remaining:
                    print(f"{person}")
                count += 1
    except FileNotFoundError:
        print(f"{filename} not found")
        exit(-1)
    return remaining


def main():
    people = read_people("personaggi.txt")
    print(f"{len(people)}")
    remaining_people = indovina(people, "domande2.txt")

    if len(remaining_people) == 1:
        print("Hai vinto")
    else:
        print("Hai perso")


if __name__ == "__main__":
    main()
