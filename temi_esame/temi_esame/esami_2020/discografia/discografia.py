"""
{
anno: [{titolo: Under Pressure, cod_artista: 01023 }]
}
"""


def leggi_file_artisti(filename):
    result = []
    try:
        with open(filename, 'r', encoding='utf-8') as file:
            for line in file:
                fields = line.rstrip().split(";")
                artista = {
                    'cod': fields[0],
                    'file': fields[1]
                }
                result.append(artista)
    except FileNotFoundError:
        print(f'{filename} not found')
        exit(-1)
    return result


def leggi_canzoni(file_artisti):
    result = {}
    for dizionario in file_artisti:
        codice = dizionario['cod']
        filename = dizionario['file']
        try:
            with open(filename, 'r', encoding='utf-8') as file:
                for line in file:
                    fields = line.rstrip().split(";")
                    anno = fields[0]
                    titolo = fields[1]

                    if anno not in result:
                        result[anno] = []
                    canzone = {'titolo': titolo, 'cod': codice}
                    result[anno].append(canzone)
        except FileNotFoundError:
            print(f'{filename} not found')
            exit(-1)

    for anno in result.keys():
        lista = result[anno]
        lista.sort(key=get_titolo)

    return sorted(result.items(), key=get_anno)


def main():
    file_artisti = leggi_file_artisti("artisti.txt")
    canzoni_dict = leggi_canzoni(file_artisti)
    for anno, canzoni in canzoni_dict:
        print(f'{anno}:')
        for canzone in canzoni:
            print(f'{canzone["titolo"]:35} {canzone["cod"]}')


def get_anno(t):
    return t[0]


def get_titolo(d):
    return d['titolo']


if __name__ == "__main__":
    main()
