def read_glucometer_data(filename):
    result = {}
    try:
        with open(filename, 'r', encoding='utf-8') as file:
            for line in file:
                # 1BF0 17:00 160 37.0 68
                [patience, timestamp, glucosium, _, _] = line.rstrip().split(' ')
                glucosium = int(glucosium)
                if glucosium >= 200:
                    if patience not in result:
                        result[patience] = {'readings': [], 'tot': 0}
                    result[patience]['readings'].append((timestamp, glucosium))
                    result[patience]['tot'] += 1
    except FileNotFoundError:
        print(f'{filename} not found.')
        exit(-1)

    return sorted(result.items(), key=get_readings, reverse=True)


def get_readings(tuple):
    return tuple[1]['tot']


def main():
    peaks = read_glucometer_data('glucometers.txt')
    """
    0AE1 21:25 201

    """
    for patient_data in peaks:
        for data in patient_data[1]['readings']:
            print(f"{patient_data[0]} {data[0]} {data[1]}")
        print(" ")


if __name__ == "__main__":
    main()
