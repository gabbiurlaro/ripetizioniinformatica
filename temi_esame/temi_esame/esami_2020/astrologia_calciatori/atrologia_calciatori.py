def leggi_zodiaco(filename):
    zodiaco = list()
    try:
        with open(filename, 'r', encoding='utf-8') as file:
            for line in file:
                [name, start, stop] = line.rstrip().split(",")
                [ds, ms] = start.split("/")
                [de, me] = stop.split("/")
                zodiaco.append({
                    'name': name,
                    'start': f"{ms}{ds}",
                    'stop': f"{me}{de}"
                })
    except FileNotFoundError:
        print(f'{filename} does not exis.')
        exit(-1)
    return zodiaco


def get_starsign(date, zodiaco):
    [d, m, a] = date.split("/")
    date = f"{m}{d}"
    for element in zodiaco:
        if element['start'] <= date < element['stop']:
            return element['name']
    return 'Capricorno'


def get_goal(a):
    return a[1]


def classifica_segni(filename, zodiaco):
    result = dict()  # key: segno, valore: il numero di goal
    try:
        with open(filename, 'r', encoding='utf-8') as file:
            for line in file:
                [_, goal, _, birth] = line.rstrip().split(",")
                sign = get_starsign(birth, zodiaco)

                if sign not in result:
                    result[sign] = int(goal)
                else:
                    result[sign] += int(goal)
    except FileNotFoundError:
        print(f'{filename} not found')
        exit(-1)
    print(result)
    return sorted(result.items(), key=get_goal, reverse=True)


def normalize(lista):
    scale_factor = lista[0][1] / 50
    result = []
    for i in range(len(lista)):
        result.append((lista[i][0], lista[i][1], int(lista[i][1] // scale_factor)))
    return result


def main():
    zodiaco = leggi_zodiaco('zodiaco.csv')
    classifica = classifica_segni('sportivi.csv', zodiaco)
    for sign, score, norm_score in normalize(classifica):
        print(f"{sign:10}\t({score})\t{'*' * norm_score}")


if __name__ == "__main__":
    main()
