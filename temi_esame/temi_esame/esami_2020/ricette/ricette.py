def leggi_cibi(filename):
    cibi = list()
    try:
        file = open(filename, 'r', encoding='utf-8')
        for line in file:
            fields = line.replace('\ufeff', '').rstrip().split(";")
            cibo = {
                'nome': fields[0],
                'costo': float(fields[1]),
                'cal': int(fields[2])
            }
            cibi.append(cibo)
        file.close()
    except FileNotFoundError:
        print(f'{filename} not found')
        exit(-1)
    return cibi


def trova_ingrediente(cibi, ingrediente):
    for cibo in cibi:
        if cibo['nome'] == ingrediente:
            return cibo['costo'], cibo['cal']
    return None


def leggi_ricetta(filename, cibi):
    numero_ingredienti = 0
    costo = 0.0
    calorie = 0.0
    try:
        with open(filename, 'r', encoding='utf-8') as file:
            for line in file:
                if "Ingredienti" in line:
                    continue
                elif line == "\n":
                    break
                else:
                    fields = line.rstrip().split(";")
                    ingrediente = fields[0]
                    qt = int(fields[1])
                    print(f"{ingrediente} - {qt}")
                    numero_ingredienti += 1
                    # troviamo l'ingrediente
                    costo_ingrediente, calorie_ingrediente = trova_ingrediente(cibi, ingrediente)
                    costo += qt / 1000 * costo_ingrediente
                    calorie = qt / 1000 * calorie_ingrediente

    except FileNotFoundError:
        print(f'{filename} does not exists')
        exit(-1)
    return numero_ingredienti, costo, calorie


def main():
    """
    1. Leggere i cibi e metterli in una struttura dati x
    2. Leggere la ricetta(solo gli ingredienti) e calcolare gli aggregati(costo totale, calorie)
    3. Stampare gli ingredienti come indicato nel programma
    4. Stampare calorie e costo totale
    """

    cibi = leggi_cibi("cibi.txt")
    # print(cibi)
    print(f"Ingredienti:")
    numero_ingredienti, costo, calorie = leggi_ricetta("polenta_concia.txt", cibi)
    print(" ")
    print(f"Numero di ingredienti: {numero_ingredienti}")
    print(f"Costo ricetta: {costo:.2f}")
    print(f"Calorie ricetta: {calorie}")


if __name__ == "__main__":
    main()
