def leggi_sala(filename):
    sala = []
    try:
        with open(filename, "r", encoding="utf-8") as file:
            for line in file:
                linea = line.rstrip().split(",")
                for i in range(len(linea)):
                    if linea[i] == "v":
                        linea[i] = " "
                    else:
                        linea[i] = "D"
                sala.append(linea)
    except FileNotFoundError:
        print(f"{filename} not found")
        exit(-1)
    return sala


def stampa_sala(sala):
    """
       ABCDEFGHIJKL
     1|   DDDDDD
     2|   DDDDDD
     3|  DDDDDDDD
     4| DDDDDDDDDD
     5| DDDDDDDDDD
     6|
     7| DDDDDDDDDD
     8| DDDDDDDDDD
     9| DDDDDDDDDD
    10| DDDDDDDDDD
    11| DDDDDDDDDD
    """
    print("   ABCDEFGHIJKL")
    for index, line in enumerate(sala):
        print(f"{index + 1:2d}|", end="")
        for x in line:
            print(f'{x}', end='')
        print(" ")


def conversione(colonna):
    lista = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"]
    for i,x in enumerate(lista):
        if x == colonna:
            return i


def marchia_covid(sala, riga, colonna):
    for dir in [(-1, 0), (-1, -1), (0, -1), (1, -1), (1, 0), (1, 1), (0, 1), (-1, +1)]:
        new_riga = riga + dir[0]
        new_col = colonna + dir[1]
        if 0 <= new_riga <= 10 and 0 <= new_col <= 12:
            if sala[new_riga][new_col] == 'D':
                sala[new_riga][new_col] = 'I'


def scegli_posto(sala, riga, colonna):
    scelto = False
    if sala[riga][colonna] == "D":
        scelto = True
        sala[riga][colonna] = 'O'
        marchia_covid(sala, riga, colonna)
    return scelto

def main():
    sala = leggi_sala("sala.txt")
    stampa_sala(sala)

    """
    Inserire riga: 3
    Inserire colonna: G
    Prenotazione accettata
    """
    riga = int(input("Inserire riga: "))
    colonna = input("Inserire colonna: ")
    while riga != 0:
        riga -= 1
        colonna = conversione(colonna)
        scelto = scegli_posto(sala, riga, colonna)
        if scelto:
            print("Prenotazione accettata")
        else:
            print("Prenotazione rifiutata")
        riga = int(input("Inserire riga: "))
        colonna = input("Inserire colonna: ")
    stampa_sala(sala)


if __name__ == "__main__":
    main()
