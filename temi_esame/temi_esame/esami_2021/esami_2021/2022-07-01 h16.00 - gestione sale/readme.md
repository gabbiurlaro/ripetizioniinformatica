# Gestionale Sale con Restrizioni
Si vuole realizzare un programma per la gestione dei posti nella sala di un cinema. 
La mappa dei posti è memorizzata nel file "sala.txt". Nel file, la lettera "s" rappresenta una sedia e la lettera "v" uno spazio vuoto (per esempio un serie di v sono un corridio). I valori della mappa sono organizzati per righe, e in ogni riga le lettere delle caselle vicine sono separate da virgole. Il file della mappa è da considerarsi come sempre corretto.

***Esempio file 'sala.txt'***

```
v,v,v,s,s,s,s,s,s,v,v,v
v,v,v,s,s,s,s,s,s,v,v,v
v,v,s,s,s,s,s,s,s,s,v,v
v,s,s,s,s,s,s,s,s,s,s,v
v,s,s,s,s,s,s,s,s,s,s,v
v,v,v,v,v,v,v,v,v,v,v,v
v,s,s,s,s,s,s,s,s,s,s,v
v,s,s,s,s,s,s,s,s,s,s,v
v,s,s,s,s,s,s,s,s,s,s,v
v,s,s,s,s,s,s,s,s,s,s,v
v,s,s,s,s,s,s,s,s,s,s,v
```

Il programma inizia stampando la mappa della sala, con la seguente notazione:
- "D": indica un posto disponibile,
- "O": indica un posto occupato,
- "I": indica un posto non utilizzabile in base alle norme anticontagio,
- spazio: indica una zona vuota (segnalata con "v" nel file di input). 

Inizialmente, tutti i posti nella mappa sono rappresentati con "D" oppure con uno spazio.
Le norme anticontagio impongono agli spettatori di sedersi distanziati di un posto l'uno dall'altro in ogni direzione (su, giù, destra, sinistra, e in diagonale).

Successivamente, il programma deve chiedere all'utente quale posto intende occupare e ne valuta la disponibilità. Il posto è identificato dalle sue coordinate:
- le righe sono indicate con numeri interi crescenti (dall'altro verso il basso). La riga più in alto è indicata con 1.  
- le colonne sono indicate con lettere in ordine alfabetico crescente (da sinistra verso destra). La colonna più a sinistra è indicata con A.

Dopo ogni inserimento, il programma deve indicare se la prenotazione è stata accettata oppure se il posto è indisponibile.

Il programma continua chiedendo ripetutamente all'utente una nuova prenotazione.
Quando l'utente inserisce '0' come numero di riga, il programma termina stampando a video la nuova mappa della sala, utilizzando la stessa notazione iniziale. 

**Esempio di Esecuzione**
```
La mappa della sala è:
   ABCDEFGHIJKI
 1|   DDDDDD  
 2|   DDDDDD  
 3|  DDDDDDDD 
 4| DDDDDDDDDD
 5| DDDDDDDDDD
 6|           
 7| DDDDDDDDDD
 8| DDDDDDDDDD
 9| DDDDDDDDDD
10| DDDDDDDDDD
11| DDDDDDDDDD

Inserire riga: 3
Inserire colonna: G
Prenotazione accettata

Inserire riga: 7
Inserire colonna: D
Prenotazione accettata

Inserire riga: 8
Inserire colonna: C
Posto indisponibile

Inserire riga: 6
Inserire colonna: J
Posto indisponibile

Inserire riga: 0
La mappa della sala è:
   ABCDEFGHIJKI
 1|   DDDDDD  
 2|   DDIIID  
 3|  DDDIOIDD 
 4| DDDDIIIDDD
 5| DDDDDDDDDD
 6|           
 7| DIOIDDDDDD
 8| DIIIDDDDDD
 9| DDDDDDDDDD
10| DDDDDDDDDD
11| DDDDDDDDDD
```
