def leggi_preferenze(filename):
    lista = {}
    attivita_preferita = 0
    try:
        with open(filename, 'r', encoding='utf-8') as f:
            for line in f:
                riga = line.rstrip().split(',')
                if riga[0] not in lista:
                    lista[riga[0]] = [riga[1]]
               # else:
                    #lista[riga[0]].append(riga[1])

    except FileNotFoundError:
        print('file non trovato!!')
        exit(-1)
    return lista


def leggi_persone(filename, preferenze):
    persone = []
    try:
        with open(filename,'r',encoding='utf-8') as f:
            for line in f:
                riga = line.rstrip().split(',')
                persona= {
                    'nome': riga[0],
                    'id': riga[1].strip(),
                    'età':int(riga[2])
                }
                persone.append(persona)
            for persona in persone:
                for id in preferenze:
                    if id == persona['id']:
                        persona['preferenza'] = preferenze[id]
    except FileNotFoundError:
        print('file non trovato!!!')
        exit(-2)
    return persone

def risoluzione(persone,x ):
    lista = []
    persona = None
    for p in persone:
        if p['id'] == x:
            persona = p
    if persona is not None:
        for person in persone:
            if person['preferenza'] == persona['preferenza'] and person['id'] != persona['id']:
                if persona['età'] + 3 > person['età'] > persona['età'] - 3:
                    lista.append(person)
        return persona, lista
    else:
        return None

def main():
    x = input('inserisci un id:')
    preferenze = leggi_preferenze('usersLoc.txt')
    persone = leggi_persone('usersAge.txt', preferenze)
    persona, simili = risoluzione(persone, x)
    print(f"affinità all'utente {x} ({persona['nome']})")
    for i in simili:
        print(f"-{i['nome']:3}")


if __name__ == '__main__':
    main()