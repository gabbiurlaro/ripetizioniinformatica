def leggi_persone(filename):
    persone = []
    try:
        with open(filename, 'r', encoding='utf-8') as f:
            for line in f:
                riga = line.rstrip().split(',')
                persona = {
                    'nome': riga[0],
                    'id': riga[1].strip(),
                    'età':int(riga[2])
                }
                persone.append(persona)
    except FileNotFoundError:
        print('file non trovato!!!')
        exit(-2)
    return persone


def get_k(t):
    return t[1]


def leggi_preferenze(persone, filename):
    dizionario = {}
    try:
        with open(filename, 'r', encoding='utf-8') as f:
            for line in f:
                fields = line.rstrip().split(',')
                id = fields[0]
                loc = fields[1]
                if id not in dizionario:
                    dizionario[id] = {}
                if loc not in dizionario[id]:
                    dizionario[id][loc] = 0
                dizionario[id][loc] += 1
            best_loc = {}
            for id, preferenze in dizionario.items():
                best_loc[id] = max(preferenze.items(), key=get_k)[0]
            for persona in persone:
                persona['preferenza'] = best_loc[persona['id']]
    except FileNotFoundError:
        print('file non trovato!!!')
        exit(-2)
    return persone

def risoluzione(persone,x ):
    lista = []
    persona = None
    for p in persone:
        if p['id'] == x:
            persona = p
    if persona is not None:
        for person in persone:
            if person['preferenza'] == persona['preferenza'] and person['id'] != persona['id']:
                if persona['età'] + 3 > person['età'] > persona['età'] - 3:
                    lista.append(person)
        return persona, lista
    else:
        return None

def main():
    x = input('inserisci un id:')
    persone = leggi_persone('usersAge.txt')
    persone = leggi_preferenze(persone, 'usersLoc.txt')
    persona, simili = risoluzione(persone, x)
    print(f"affinità all'utente {x} ({persona['nome']})")
    if not simili:
        print("Non ci sono affinità con questo utente")
    else:
        for i in simili:
            print(f"-{i['nome']:3}")

if __name__ == "__main__":
    main()