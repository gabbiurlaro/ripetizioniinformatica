# Incroci
Si scriva un programma in grado di gestire la creazione delle possibili affinità di ogni utente iscritto al social network "VeryFriend". 

Le informazioni personali degli utenti sono salvate in due diversi files:

- usersAge.txt: per ogni riga indica `<nome>, <ID>, <eta'>` di un diverso utente. Il file non presenta errori né ripetizioni.
- usersLoc.txt: contiene l'elenco delle attività svolte dagli utenti in ordine sparso, nel formato `<ID>, <attivita'>`. Ciascuna attività può comparire più volte per uno stesso utente: significa che l'utente ha svolto l'attività più volte in tempi diversi. Per ogni utente, si supponga che ci sia sempre un massimo tra le occorrenze delle attività.

Il programma deve chiedere all'utente di inserire da tastiera un ID utente, e poi deve fornire la lista degli utenti più affini a tale utente. La crezione della lista delle affinità si basa sulle seguenti caratteristiche:

- la differenza di età dei due utenti è compresa nell'intervallo +/-3
- il luogo preferito dai due utenti è lo stesso (preferito = visitato più volte) 

Il programma termina dopo aver elaborato la richiesta relativa all'ID inserito dall'utente.



***Esempio file usersAge.txt (in input)***
```
Reno Roger, 1010, 22
Gina Ginger, 1011, 23
Enio Anio, 1100, 24
Anna Pannocchia, 1903, 21
Billi Ballo, 1900, 23
Filippo Tip, 0200, 29 
```
***Esempio file usersLoc.txt (in input)***
```
0200, stadio
1900, disco
1900, museo
1900, disco
1900, disco
0200, disco
1010, disco
1010, disco
0200, stadio
1100, cinema
1100, disco
1900, disco
1010, museo
1011, museo
1900, museo
1903, disco
1100, cinema
1100, cinema
```
***Esempio di esecuzione***
```
c:> python main.py

Inserire id utente: 1010

Lista affinità per utente 1010 (Reno Roger):
- Anna Pannocchia
- Billi Ballo

c:> python main.py

Inserire id utente: 1011

Lista affinità per utente 1011 (Gina Ginger):
Non ci sono affinità con questo utente
```
