# Costo del lavoro
In un'azienda, ogni dipendente ha il suo ruolo, e ogni ruolo ha una diversa retribuzione.
Scrivete un programma che calcoli ore lavorate e costo del personale in un dato mese, inserito dall'utente.

Nello specifico, vi sono forniti i seguenti file:
 - il file 'presenze.txt' è scritto nel formato <nome_dipendente>,\<data>,<ore_lavorate>
- il file 'ruolo.txt' è scritto nel formato <nome_dipendente>,<ruolo_in_azienda>
- il file 'retribuzione.txt' è scritto nel formato <ruolo_in_azienda>,<tariffa_oraria>

Scrivete un programma che chieda all'utente di inserire un mese di riferimento, in formato numerico (ad esempio 01, 02, 11) e stampi a schermo le seguenti informazioni per il mese di riferimento:
- totale ore lavorate 
- ore lavorate da ciascun dipendente, in ordine decrescente
- costo totale lavoro
- costo di ciascun dipendente, in ordine decrescente

Assumete che l'anno di riferimento sia sempre il 2021

***Esempio file 'presenze.txt'***
```
mario rossi,01/06/2021,8
davide profumo,01/07/2021,8
silvia pettirosso,01/07/2021,6
silvia pettirosso,01/07/2021,2
mario rossi,02/07/2021,8
davide profumo,02/07/2021,8
silvia pettirosso,02/07/2021,6
silvia pettirosso,02/07/2021,2
luigi bartoli,02/07/2021,8
luigi bartoli,01/08/2021,8 
```

***Esempio file 'ruolo.txt'***
```
mario rossi,dipendente qualificato
davide profumo,apprendista
silvia pettirosso,dipendente qualificato
luigi bartoli,stagista
```

***Esempio file 'retribuzione.txt'***
```
dipendente qualificato,20
apprendista,10
stagista,5
```

***Esempio di output***
```
Inserisci mese di riferimento: 07

Totale ore lavorate nel mese 07: 48

Ore lavorate per dipendente nel mese 07: 
davide profumo: 16 ore
silvia pettirosso: 16 ore
mario rossi: 8 ore
luigi bartoli: 8 ore

Totale costo lavoro nel mese 07: 680

Costo del lavoro per dipendente nel mese 07: 
silvia pettirosso: 320 euro
davide profumo: 160 euro
mario rossi: 160 euro
luigi bartoli: 40 euro
```