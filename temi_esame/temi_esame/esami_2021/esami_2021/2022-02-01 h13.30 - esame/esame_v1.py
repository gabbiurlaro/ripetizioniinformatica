def leggi_file(filename):
    studenti = []
    try:
        with open(filename, 'r', encoding='utf-8') as file:
            file.readline()
            for line in file:
                fields = line.strip().split(",")
                somma = int(fields[3]) + int(fields[4]) + int(fields[5])
                if somma < 18:
                    voto = 'INS'
                elif somma > 30:
                    voto = '30L'
                else:
                    voto = somma
                studente = {
                    'matricola': fields[0],
                    'voto': voto,
                    'somma': somma
                }
                studenti.append(studente)
    except FileNotFoundError:
        print(f'{filename} not found')
        exit(-1)
    return studenti


def calcola_statistiche(studenti):
    result = {'num_studenti': len(studenti)}
    somma = 0
    num_sufficienti = 0
    for studente in studenti:
        somma += studente['somma']
        if studente['voto'] != 'INS':
            num_sufficienti += 1
    result['media'] = somma / len(studenti)
    result['sufficienti'] = num_sufficienti
    result['insufficienti'] = len(studenti) - num_sufficienti
    return result


def main():
    """
    Risultati anonimizzati
    024321:INS
    123456:30
    185007:25
    483419:INS
    786737:30L
    """
    studenti = leggi_file("risultati.csv")
    print("Risultati anonimizzati")
    for studente in studenti:
        print(f"{studente['matricola']}:{studente['voto']}")
    """
    Statistiche d'esame
    -Numero esaminandi: 5
    -Insufficienti: 2 (40%)
    -Sufficienti: 3 (60%)
    -Punteggio medio: 19.6/30
    """
    print("Statistiche d'esame")
    statistiche = calcola_statistiche(studenti)
    """
    {'num_studenti': 5, 'ins': 2, 'media': 19.6}
    [('num_studenti', 5), ('ins', 2), ...]
    """
    print(f"-Numero esaminandi: {statistiche['num_studenti']}")
    print(f"-Insufficienti: {statistiche['insufficienti']} ({statistiche['insufficienti']/statistiche['num_studenti']:.0%})")
    print(f"-Sufficienti: {statistiche['sufficienti']} ({statistiche['sufficienti']/statistiche['num_studenti']:.0%})")
    print(f"-Punteggio medio: {statistiche['media']:.1f}/30")


if __name__ == "__main__":
    main()
