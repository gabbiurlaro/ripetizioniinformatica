## Esame

Un file risultati.csv riporta i risultati di un esame scritto.
Nello specifico, il file riporta sulla prima riga l'intestazione con i nomi dei campi e, nelle righe successive, i dati relativi agli studenti che hanno partecipato all'esame, nel seguente formato:
```
MATRICOLA,COGNOME,NOME,PARTE A,PARTE B,PARTE C
123456,ROSSI,ANTONIO,11,10,9
024321,VERDI,GABRIELE,4,3,1
...
```

Si facciano le seguenti assunzioni:

- i campi sono tutti separati da virgole
- il campo MATRICOLA è il numero di matricola dello studente (identificativo univoco di 6 cifre intere)
- i campi COGNOME e NOME sono rispettivamente il cognome e il nome dello studente (possono contenere spazi)
- i campi PARTE A, PARTE B e PARTE C sono rispettivamente i punteggi parziali ottenuti dallo studente nelle tre parti di cui si compone l'esame. Ciascuno di questi punteggi è un intero compreso tra un minimo di 0 e un massimo di 11. 
- il file non è vuoto e non contiene errori di formato
- gli studenti non sono riportati in un ordine specifico

Si scriva un programma Python che calcoli e stampi a video il voto ottenuto da ciascuno studente, con i seguenti accorgimenti:

- se la somma dei punteggi parziali delle tre parti è compresa tra 18 e 30, il voto finale coincide con tale somma
- se il punteggio totale è inferiore a 18, il voto finale è 'INS' (insufficiente)
- se il punteggio totale è superiore a 30, il voto finale è '30L' (30 e lode)

I voti devono essere riportati in ordine di matricola e in forma anonimizzata. In particolare, si deve riportare su ogni riga soltanto la matricola di uno studente e il relativo voto, separati da un carattere ':'. 

Una volta riportato tutto l'elenco, il programma deve anche stampare a video le seguenti statistiche:

- il numero totale degli esaminandi
- il numero (e la percentuale, arrotondata all'intero più vicino) di studenti che hanno ricevuto un'insufficienza 
- il numero (e la percentuale, arrotondata all'intero più vicino) di studenti che hanno ricevuto una sufficienza 
- il punteggio medio in trentesimi, riportato con una sola cifra decimale

 ### Esempio file di input risultati.csv:
```
MATRICOLA,COGNOME,NOME,PARTE A,PARTE B,PARTE C
123456,ROSSI,ANTONIO,11,10,9
024321,VERDI,GABRIELE,4,3,1
185007,BIANCHI,FRANCESCA,11,9,5
786737,GIALLI,ANNA MARIA,11,11,11
483419,VERDE CHIARO,FABIO,0,2,0
```
 ### Esempio di output a video:
```
Risultati anonimizzati
024321:INS
123456:30
185007:25
483419:INS
786737:30L

Statistiche d'esame
-Numero esaminandi: 5
-Insufficienti: 2 (40%)
-Sufficienti: 3 (60%)
-Punteggio medio: 19.6/30
```