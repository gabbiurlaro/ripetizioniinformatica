from operator import itemgetter

def leggi_gara(nome_file):
    gara = []
    file = open(nome_file, 'r', encoding='utf-8')
    for riga in file:
        campi = riga.rstrip('\n').split(';')
        record = {
            'disciplina': campi[0],
            'timestamp': int(campi[1]),
            'battiti': int(campi[2]),
            'velocita': int(campi[3])
        }
        gara.append(record)
    file.close()
    return gara

def leggi_dati(nome_file):
    dati = {}
    file = open(nome_file, 'r', encoding='utf-8')
    for riga in file:
        campi = riga.rstrip('\n').split('-')
        dati['nome'] = campi[0]
        dati['cognome'] = campi[1]
        dati['anni'] = int(campi[2])
        dati['altezza'] = int(campi[3])
        dati['peso'] = int(campi[4])
        dati['numero_finestre'] = int(campi[5])

    file.close()
    return dati

def calcola_battito_medio(gara):
    battito_totale = 0
    for disciplina in gara:
        battito_totale = battito_totale + disciplina['battiti']
    battito_medio = battito_totale / len(gara)
    return battito_medio

def calcola_velocita_media(gara):
    velocita_totale = 0
    for disciplina in gara:
        velocita_totale = velocita_totale + disciplina['velocita']
    velocita_media = velocita_totale / len(gara)
    return velocita_media

def tempo_in_ore(battito_maggiore):
    secondi = battito_maggiore['timestamp']
    minuti, secondi = divmod(secondi, 60)
    ore, minuti = divmod(minuti, 60)
    return ore, minuti, secondi

# 3. Il "livello" di fatica, che dipende dalla categoria in cui il battito cardiaco misurato rientra piu' volte
# (scegliere la piu' alta in caso di parita')
    # lieve: bpm<=100
    # aerobico 100<bpm<150
    # anaerobico bpm>150
def calcola_livello_fatica(gara):
    contatore_lieve = 0
    contatore_aerobico = 0
    contatore_anaerobico = 0
    for disciplina in gara:
        if disciplina['battiti'] <= 100:
            contatore_lieve += 1
        elif 100 < disciplina['battiti'] < 150:
            contatore_aerobico += 1
        elif disciplina['battiti'] > 150:
            contatore_anaerobico += 1
    if contatore_lieve >= contatore_aerobico and contatore_lieve >= contatore_anaerobico:
        return print(f"L'allenamento e' classificato come: lieve")
    elif contatore_aerobico >= contatore_lieve and contatore_aerobico >= contatore_anaerobico:
        return print(f"L'allenamento e' classificato come: aerobico")
    elif contatore_anaerobico >= contatore_lieve and contatore_anaerobico >= contatore_aerobico:
        return print(f"L'allenamento e' classificato come: anaerobico")

# 4. Il numero di volte in cui il battito massimo consigliato è stato superato per almeno Numerofinestre campionamenti
# consecutivi, NON si considerino finestre con elementi in comune, il valore Numerofinestre si trova nel file della
# persona. Il battito massimo consigliato viene calcolato come bpm_MAX=220-Anni
def bpm_MAX_superato(gara, dati):
    volte = 0
    bpm_MAX = 220 - dati['anni']
    posizione = 0
    for disciplina in gara:
        if disciplina['battiti'] >= bpm_MAX:
            while disciplina['battiti'] >= bpm_MAX and posizione <= 2:
                volte += 1
                posizione += 1
        else:
            posizione += 1
    return volte

def main():
    gara = leggi_gara('gara2022.txt')
    # print(gara) --> stampa di controllo
    dati = leggi_dati('persona.txt')
    # print(dati) --> stampa di controllo
    battito_medio = calcola_battito_medio(gara)
    print(f"Battito medio: {battito_medio:.2f}")
    velocita_media = calcola_velocita_media(gara)
    print(f"Velocità media: {velocita_media:.2f}")
    battito_maggiore = max(gara, key=itemgetter('battiti'))
    ore, minuti, secondi = tempo_in_ore(battito_maggiore)
    print(f"Il battito cardiaco piu' elevato ({battito_maggiore['battiti']}) e' stato raggiunto dopo "
          f"{ore} ore {minuti} minuti {secondi} secondi, per la disciplina {battito_maggiore['disciplina']}")
    calcola_livello_fatica(gara)
    volte = bpm_MAX_superato(gara, dati)
    print(f"Il battito massimo personale e' stato superato per {volte} finestra/e")

if __name__ == "__main__":
    main()