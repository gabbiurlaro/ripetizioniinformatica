# Parametri vitali

Si scriva un programma in python che analizzi il contenuto di un file di testo 'gara2022.txt',
contenente i parametri vitali di un atleta di triathlon durante una gara.
Lo smartwatch usato nello specifico campiona i seguenti parametri ogni 30 secondi, con il seguente formato:

	disciplina;timestamp;battiti_cardiaci;velocita'

Il campo disciplina puo' essere soltanto 'Nuoto','Corsa','Ciclismo'. 
Il campo timestamp riporta i secondi passati dall'inizio della gara (intero),
battiti_cardiaci e' un intero (in battiti per minuto - bpm), e velocita' un
numero intero (in km/h).

Tutte le misurazioni di una disciplina sono sempre consecutive e sempre
ordinate per timestamp, e le tre discipline vengono ovviamente svolte in
sequenza (per esempio, prima 'Nuoto', poi 'Corsa', poi 'Ciclismo'). 
Si assuma che il contenuto del file sia corretto e senza dati mancanti.

Per dare consigli personalizzati lo smartwatch legge inoltre da un file
'persona.txt' i dati dell'atleta, salvati nel seguente formato su un'unica
riga:
       
	Nome-Cognome-Anni-Altezza-Peso-Numerofinestre

dove altezza e' un intero (in cm), Peso un numero reale (in kg), e
NumeroFineste un intero.
Si assuma che il file sia sempre corretto.


Il programma deve riportare a video le seguenti informazioni:

1. Il battito cardiaco e la velocita' media durante tutta la gara
2. I dati di quando e' stato raggiunto il battito cardiaco piu' elevato, stampato con il seguente formato:
3. Il "livello" di fatica, che dipende dalla categoria in cui il battito cardiaco misurato rientra piu' volte (scegliere la piu' alta in caso di parita')
	- lieve: bpm<=100
	- aerobico 100<bpm<150
	- anaerobico bpm>150
4. Il numero di volte in cui il battito massimo consigliato e' stato superato per almeno Numerofinestre campionamenti consecutivi, NON si considerino finestre con elementi in comune, il valore Numerofinestre si trova nel file della persona.
Il battito massimo consigliato viene calcolato come      bpm_MAX=220-Anni

ATTENZIONE: Si stampino solo le prime due cifre dopo la virgole a terminale

# Esempio file di input 'gara2022.txt'
	Nuoto;0;180;6
	Nuoto;30;200;7
	Nuoto;60;200;7
	Nuoto;90;200;7
	Nuoto;120;150;8
	Corsa;150;170;8
	Corsa;180;210;8
	Ciclismo;210;220;40
	Ciclismo;240;210;20

# Esempio file di input 'persona.txt':
	Mario-Rossi-30-190-95-3

# Output del programma nel caso dell'esempio
	Il battito medio e' 193.33
	La velocita' media e' 12.33
	Il battito cardiaco piu' elevato (220) e' stato raggiunto dopo 0 ore 3 minuti 30 secondi, per la disciplina Ciclismo
	L'allenamento e' classificato come: anaerobico
	Il battito massimo personale e' stato superato per 2 finestra/e


FILES
gara2022.txt
persona.txt

