## Diario alimentare

Un centro di ricerca nutrizionale ha un database di cibi con relativi valori nutrizionali salvati su un file con nome ***nutrients.txt***, che riporta la composizione di cibi in termini di macro-nutrienti, con un cibo per riga. Ogni riga del file ha il seguente formato:

***\<nome\> \<carboidrati\> \<proteine\> \<grassi\>***

Dove:
-	***\<nome\>*** il nome del cibo, eventuali spazi nel nome sono rappresentati come caratteri underscore ('_'),
-	***\<carboidrati\>***  il numero (intero) di grammi di carboidrati per ogni 100 grammi di cibo 
-	***\<proteine\>***  il numero (intero) di grammi di proteine per ogni 100 grammi di cibo 
-	***\<grassi\>***  il numero (intero) di grammi di grassi per ogni 100 grammi di cibo 

Per ogni riga, i singoli campi sono separati da uno spazio. Si assume che il **formato del file sia corretto** e che i **nomi** dei cibi **non siano duplicati** all’interno del file.

Il centro di ricerca colleziona i diari alimentari di ogni paziente come file di testo riportante, per ogni giorno dell’anno, il peso corporeo e l’elenco dei cibi assunti durante tutta la giornata. Il diario alimentare ha una struttura a blocchi giornalieri. Il file riporta una sequenza di N blocchi giornalieri, con N **non noto a priori**. Ogni blocco è relativo ad una giornata, ed inizia con una riga contenente esclusivamente la stringa \*\*\* (ovvero, 3 caratteri '\*' consecutivi).

La prima riga di ogni blocco riporta due dati separati da uno spazio:
-	***\<numero_giorno\>*** numero progressivo che può assumere valori nell'intervallo [1-366]
-	***\<peso\>*** il peso del paziente espresso in kg (numero reale).

Le successive righe del blocco riportano i dati sui cibi assunti:
-	***\<nome_cibo\>*** il nome di un cibo assunto durante la giornata
-	***\<grammi_assunti\>*** il numero di grammi assunti (numero intero).

Non è noto a priori quante siano le righe corrispondenti ai cibi assunti nei singoli blocchi. Si assume che **tutti i cibi** riportati nel file contenente il diario alimentare del paziente siano **presenti** in nutrients.txt.

Scrivere un programma in Python che acquisisca il nome di un file contenente il diario alimentare di un paziente e fornisca la valutazione generale e giornaliera del paziente, applicando il seguente criterio: la valutazione generale può essere: 

- **ACCEPTABLE** se le giornate in cui le calorie assunte superano il fabbisogno giornaliero sono minori di quelle in cui questo non accade
- **NOT_ACCEPTABLE**, in caso contrario.

Si considerino le seguenti assunzioni:
-	1g di carboidrati corrisponde a 4kcal
-	1g di proteine corrisponde a 4kcal
-	1g di grassi corrisponde a 9kcal
-	il calcolo del fabbisogno calorico giornaliero (FG) si può semplificare come FG<sub>(kcal)</sub> = peso<sub>(kg)</sub> \* 30

Il programma deve fornire in output le seguenti informazioni:
-	una riga che indichi se il comportamento generale del paziente è accettabile o meno, 
-	la media delle calorie assunte e la media del fabbisogno calorico giornaliero
-	una riga di valutazione per ogni giorno riportato nel file specificato che indichi il totale delle kcal assunte confrontate con il fabbisogno calorico giornaliero (i.e., includendo l'indicazione della relazione tra i due: <, > o =).

Tutte le quantità numeriche devono essere riportate in output con due cifre decimali. 


***Esempio di file nutrients.txt***
```
pasta_in_bianco 75 15 5
carbonara 41 15 35
petto_di_pollo 0 20 1
yogurt_greco 3 10 0
```

***Esempio di diario alimentare (diary.txt)***
```
***
1 85
yogurt_greco 100
carbonara 250
petto_di_pollo 300
pasta_in_bianco 250
***
2 87
petto_di_pollo 400
pasta_in_bianco 400
***
3 86
yogurt_greco 250
carbonara 250
petto_di_pollo 400
```

***Esempio di file di output considerando come diario alimentare diary.txt***
```
ACCEPTABLE
2162.83 2580.00
1 2679.00 > 2550.00
2 1976.00 < 2610.00
3 1833.50 < 2580.00
```