## Strategia militare

Il conte von Bülow ha pubblicato un nuovo manuale di strategia militare in cui dimostra come le guerre si vincano seguendo precisi procedimenti matematici e l’inutilità delle battaglie. Per mettere alla prova le sue teorie ha bisogno di un programma informatico che permetta di calcolare statistiche sui battaglioni.
Il file schieramento.txt contiene la rappresentazione di un piano rettangolare. Gli 0 rappresentano un'area vuota. Gli 1 la prima linea di uno schieramento, i 2 la seconda e così via fino ad un massimo di 9. Le file hanno tutte la stessa larghezza, ma in quelle dopo la prima possono esserci dei buchi dovuti ai caduti (non ai bordi della fila, e non nella prima)

Vi si richiede di scrivere un programma python che acquisisca lo schieramento e calcoli e visualizzi:

- la larghezza dello schieramento
- il numero di file
- la direzione (Nord, Est, Sud o Ovest)
- la fila con più buchi (se ci fossero buchi)

La formazione ha almeno due file.

***Esempio***
```
00000000
32100000
00100000
02100000
32100000
00000000
00000000
00000000
```

***output***
```
larghezza: 4
numero di file: 3
direzione: Est
fila con più buchi: 3
```