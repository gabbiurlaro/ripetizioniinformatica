def leggi_risoiste(filename):
    alunni = {}
    try:
        with open(filename, 'r', encoding='utf-8') as f:
            for line in f:
                riga = line.rstrip().split()
                alunni[riga[0]] = riga[1:]
    except FileNotFoundError:
        print('fie non trovato!!!!')
        exit(-1)
    return alunni


def leggi_posizioni(filename):
    disposizione = []
    try:
        with open(filename, 'r', encoding='utf-8') as f:
            for line in f:
                riga = line.rstrip().split()
                disposizione.append(riga)
    except FileNotFoundError:
        print('file non trovato !!!!')
        exit(-1)
    return disposizione


"""
le risposte di due studenti vicini sono esattamente uguali: sia le risposte date (indicate con 'A', 'B', 'C', 'D') sia le risposte non date (indicate con '-') sono le stesse. In questo caso non è possibile stabilire chi abbia copiato, e il programma stampa il messaggio "Le risposte di studente1 e studente2 sono le stesse" (studente1 e studente2 devono essere sostituiti con i nomi reali degli studenti).

le risposte date da studente1 (indicate con 'A', 'B', 'C', 'D') sono identiche alle risposte di studente2, ma studente2 ha dato almeno una risposta in più (ovvero, almeno un caso in cui la corrispondente risposta di studente1 è '-'). In questo caso si sospetta che studente1 abbia copiato da studente2 e il programma stampa il messaggio "studente1 può aver copiato da studente2".

le risposte date da studente1 hanno almeno una differenza rispetto alle risposte data da studente2 (ovvero, ci sono domande a cui entrambi gli studenti hanno risposto e le loro risposte sono diverse). In questo caso non si sospetta copiatura e il programma non stampa nulla.
"""


def check_uguali(a, b):
    if a == b:
        return True
    else:
        return False


def check_possibile_copiatura(risposte_a, risposte_b):
    copiato = True
    for i in range(len(risposte_a)):
        if risposte_a[i] != "-":
            if risposte_a[i] != risposte_b[i]:
                copiato = False
                break
    return copiato


def controllo(risposte, disposizioni):
    for fila in disposizioni:
        for i in range(len(fila)):
            if i == 0:
                if check_uguali(risposte[fila[i]], risposte[fila[i + 1]]):
                    print(f"Le risposte di {fila[i]} e {fila[i + 1]} sono le stesse.")
                elif check_possibile_copiatura(risposte[fila[i]], risposte[fila[i + 1]]):
                    print(f"{fila[i]} può aver copiato da {fila[i + 1]}.")
            if i == len(fila) - 1:
                if check_possibile_copiatura(risposte[fila[i]], risposte[fila[i - 1]]):
                    print(f"{fila[i]} può aver copiato da {fila[i - 1]}.")
            else:
                if check_uguali(risposte[fila[i]], risposte[fila[i + 1]]):
                    print(f"Le risposte di {fila[i]} e {fila[i + 1]} sono le stesse.")
                else:
                    if check_possibile_copiatura(risposte[fila[i]], risposte[fila[i - 1]]):
                        print(f"{fila[i]} può aver copiato da {fila[i - 1]}.")
                    elif check_possibile_copiatura(risposte[fila[i]], risposte[fila[i + 1]]):
                        print(f"{fila[i]} può aver copiato da {fila[i + 1]}.")


def main():
    risposte = leggi_risoiste('risposte.txt')
    print(risposte)
    disposizione = leggi_posizioni('posizioni.txt')
    print(disposizione)
    controllo(risposte, disposizione)


if __name__ == '__main__':
    main()
