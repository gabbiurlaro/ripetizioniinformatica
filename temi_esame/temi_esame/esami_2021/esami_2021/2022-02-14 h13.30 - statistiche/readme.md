# Esame "Statistiche"

Si scriva un programma in linguaggio Python per il calcolo di statistiche sui dati forniti dalla Protezione Civile relativi all'andamento COVID-19 in Italia. Il file contiene diverse righe (una riga per ogni giorno a partire dal 24/02/2020). Per ogni riga sono riportati, nell'ordine: la data, i ricoverati con sintomi, i ricoverati in terapia intensiva e il totale degli ospedalizzati, separati da un punto e virgola.

Il programma deve:

- **leggere l'interrogazione dell'utente** riportata in un file di testo. Questo file riporta, nella prima riga, l'interrogazione nel formato:

`Dato-da-analizzare;mese-per-il-quale-si-vogliono-analizzare-i-dati`

- **leggere i dati giornalieri dal file "dati.txt"** (i dati sono riportati nel formato descritto in precedenza). Eventuali eccezioni in lettura da file sono da gestire

- **estrarre** dal file i dati di interesse per l'anno 2020 e per l'anno 2021

- **stampare** **una descrizione dei dati** che verranno visualizzati di seguito (in base al tipo di interrogazione)

- s**tampare i dati estratti**, riportando per ogni riga i dati di una singola giornata visualizzando prima la data, poi il dato relativo al 2020, e, infine, il dato relativo al 2021

- stampare, al termine dell'elenco, un'ulteriore riga, che riporti la differenza tra il dato rilevato l'ultimo giorno del mese, e il dato rilevato il primo giorno del mese, relativi al 2020 e al 2021, per il mese di riferimento.

Per semplicità, si ipotizzi che l'utente effettui solo interrogazioni relative a mesi in cui sono disponibili tutti i dati per entrambi gli anni.

Nel folder ci sono 3 possibili file di prova (interrogazione1.txt, interrogazione2.txt e interrogazione3.txt) e il file dati.txt.

# Esempio di Esecuzione

Esempio basato sul file `interrogazione1.txt`:
```
Confronto totale_ospedalizzati del mese di luglio
                 2020       2021
  1 luglio       1112       1761
  2 luglio       1045       1682
  3 luglio       1035       1598
  4 luglio       1011       1561
  5 luglio       1019       1528
  6 luglio       1018       1458
  7 luglio       1010       1414
  8 luglio        970       1377
  9 luglio        940       1336
 10 luglio        909       1308
 11 luglio        893       1295
 12 luglio        844       1307
 13 luglio        833       1285
 14 luglio        837       1259
 15 luglio        854       1242
 16 luglio        803       1249
 17 luglio        821       1273
 18 luglio        807       1292
 19 luglio        792       1350
 20 luglio        792       1359
 21 luglio        781       1354
 22 luglio        772       1392
 23 luglio        762       1459
 24 luglio        759       1512
 25 luglio        772       1570
 26 luglio        779       1694
 27 luglio        785       1800
 28 luglio        789       1868
 29 luglio        769       1924
 30 luglio        795       2013
 31 luglio        757       2065
Differenza       -355        304
```
