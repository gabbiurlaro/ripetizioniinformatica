from pprint import pprint
from random import randint


def leggi_tabellone(filename):
    tabellone = {}
    try:
        with open(filename, 'r', encoding='utf-8') as file:
            for line in file:
                fields = line.strip().split()
                tabellone[int(fields[1])] = fields[0]
    except FileNotFoundError:
        print(f"{filename} not found")
        exit(-1)
    return tabellone

def main():
    tabellone = leggi_tabellone('tabellone.txt')
    pos = (0, 0)
    giocatori = ["Player1", "Computer"]
    turn = randint(0, 1)
    flag = True
    bloccato = 0
    while flag:
        d1 = randint(0, 6)
        d2 = randint(0, 6)
        if bloccato > 0:
            pass
        else:
            pass

        if turn == 0:
            turn = 1
        else:
            turn = 0


if __name__ == "__main__":
    main()