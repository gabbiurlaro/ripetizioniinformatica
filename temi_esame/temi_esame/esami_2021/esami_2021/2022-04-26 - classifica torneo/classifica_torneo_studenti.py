
def get_punti(t):
    return t[1]['punti']

def leggi_file(filename):
    risultati = dict()
    try:
        with open(filename, 'r',encoding='utf-8') as f:
            for linea in f:
                riga = linea.rstrip().replace('-', ':').split(':')
                squadra1 = riga[0].strip()
                squadra2 = riga[1].strip()
                punti1 = int(riga[2])
                punti2 = int(riga[3])
                if punti1 > punti2:
                    pp1=3
                    pp2=1
                elif punti2==punti1:
                    pp1=2
                    pp2=2
                else:
                    pp1=1
                    pp2=3
                if squadra1 in risultati:
                    risultati[squadra1]['giocate']= risultati[squadra1]['giocate']+1
                    risultati[squadra1]['punti']= risultati[squadra1]['punti']+pp1
                    risultati[squadra1]['PF']= risultati[squadra1]['PF']+punti1
                    risultati[squadra1]['PS']= risultati[squadra1]['PS']+punti2

                else:
                    risultati[squadra1]= {
                        'giocate':1,
                        'punti':pp1,
                        'q':0,
                        'PF':punti1,
                        'PS': punti2
                    }
                if squadra2 in risultati:
                    risultati[squadra2]['giocate']= risultati[squadra2]['giocate']+1
                    risultati[squadra2]['punti']= risultati[squadra2]['punti']+pp2
                    risultati[squadra2]['PF']= risultati[squadra2]['PF']+punti2
                    risultati[squadra2]['PS']= risultati[squadra2]['PS']+punti1

                else:
                    risultati[squadra2]= {
                        'giocate': 1,
                        'punti': pp2,
                        'q': 0,
                        'PF': punti2,
                        'PS': punti1
                    }
    except FileNotFoundError:
        print('file non trovato')
        exit(-1)
    return risultati
def get_q(t):
    return t[1]['q']
def leggi_classifica(squadre):
    for squadra in squadre:
        squadre[squadra]['q'] = squadre[squadra]['PF']/squadre[squadra]['PS']
    #ordino secondo q
    ordinato= sorted(squadre.items(), key=get_q, reverse=True)
    #ordino secondo punti
    finale= sorted(ordinato, key=get_punti, reverse=True)
    return finale


def main():
    squadre = leggi_file('torneo.txt')
    classifica = leggi_classifica(squadre)
    print('squadre:   giocate:   punti:   Q:     PF:      PS:     ')
    print('-'*50)
    for squadra in classifica:
        print(f"{squadra[0]:20s}  {squadra[1]['giocate']:2d} {squadra[1]['punti']:3d}  {round(squadra[1]['q'],2):2f} {squadra[1]['PF']:3d} {squadra[1]['PS'] :3d} ")
                        # s sta per stringa               #d sta per nuero intero.                           #f sta per floatingpoint

main()
