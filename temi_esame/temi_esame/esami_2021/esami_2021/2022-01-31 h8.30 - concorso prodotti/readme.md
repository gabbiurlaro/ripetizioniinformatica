# Concorso Prodotti

Si realizzi un programma in Python che gestisca un concorso per premiare la qualità di una serie di prodotti. Tutte le informazioni necessarie sono contenute in un file. Ogni riga del file ha il seguente formato:

```
codice_prodotto, categoria_prodotto, costo_prodotto, nazione_prodotto, e 5 punteggi assegnati dai valutatori.
```

Si facciano le seguenti assunzioni:

Il numero di righe del file non è noto a priori
I campi codice_prodotto e categoria_prodotto non contengono spazi
Il costo_prodotto è codificato come numero con virgola.
La sigla della nazione è sempre codificata su 3 lettere maiuscole
Sono sempre assegnati 5 voti per ogni prodotto, separati da uno spazio e il valore può variare da un inimo di 0 ad un massimo di 10.
Il programma deve stampare:

Il nome del prodotto vincente (con punteggio piu alto in assoluto). 
Nel computo dei punti totali, per ogni prodotto vanno sempre SCARTATI il punteggio massimo e quello minimo tra i 5 assegnati. Il punteggio finale di ciascun prodotto è quindi dato dalla somma dei tre punteggi rimanenti.
La classifica delle prime 3 nazioni. Per ogni nazione il punteggio totale è calcolato sommando i punteggi di tutti i suoi prodotti (sempre scartando il punteggio maggiore e quello minore di ogni prodotto). Si assuma che le nazioni rappresentate non siano più di 20. 
Il prodotto col miglior punteggio di ciascuna categoria (sempre scartando il punteggio maggiore e quello minore di ogni prodotto). Si assuma che le categorie rappresentate non siano più di 20.

Esempio:

```
SKU012345 Coltello 7.50 ITA 9.3 8.9 9.7 9.7 9.8
SKU012346 Piatto 10.00 ITA 9.0 9.0 9.0 9.2 9.5 
SKU012347 Bicchiere 8.30 USA 8.4 8.7 8.5 8.6 9.0 
SKU012348 Piatto 7.30 RUS 8.3 8.7 9.5 9.6 9.0 
SKU012349 Bicchiere 5.10 GRB 8.2 8.9 8.9 8.6 9.3 
SKU012350 Forchetta 5.00 USA 8.2 8.9 8.9 8.6 9.3 
SKU012351 Coltello 7.20 GRB 8.0 8.0 8.0 8.0 8.0
```

l'output del programma dovrà essere il seguente:

```

Il miglior prodotto è: SKU012345 Coltello con un punteggio di 28.70000000000001

Classifica nazioni: 
1  classificato: ITA punteggio: 55.90
2  classificato: USA punteggio: 52.20
3  classificato: GRB punteggio: 50.40

Classifica Categorie: 
	 BICCHIERE  prod.  SKU012349 26.4  punti
	 COLTELLO  prod.  SKU012345 28.7  punti
	 FORCHETTA  prod.  SKU012350 26.4  punti
	 PIATTO  prod.  SKU012346 27.2  punti

```
