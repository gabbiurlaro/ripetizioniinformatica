"""
Rileggere il codice per trovare eventuali ottimizzazioni(ad esempio, una volta ordinata la lista, per fare max basta che prendi il primo/ultimo elemento)

"""
import operator

def conversione(a):
    lista_interi = []
    for i in a:
        b = float(i)
        lista_interi.append(b)
    return lista_interi

def ordina(l):
    return l['punteggi']

def leggi_file(file_name):
    lista_prodotti = []
    try:
        with open(file_name, 'r', encoding='utf-8') as f:
            for line in f:
                riga = line.rstrip().split()
                punteggi = conversione(riga[4:])
                minimo = min(punteggi)
                massimo = max(punteggi)
                prodotto = {
                    'codice': riga[0],
                    'categoria': riga[1],
                    'nazione': riga[3],
                    'punteggi': round(sum(punteggi)-minimo-massimo, 2)
                }
                lista_prodotti.append(prodotto)
            ordinata = sorted(lista_prodotti, key=ordina, reverse=True)
            vincente = ordinata[0]
    except FileNotFoundError:
        print('file non trovato')
        exit(-1)
    return ordinata, vincente

def ordina_d(d):
    return d[1]

def classifica_nazzioni(l):
    nazioni = {}
    for prodotto in l:
        if prodotto['nazione'] not in nazioni:
            nazioni[prodotto['nazione']] = prodotto['punteggi']
        else:
            nazioni[prodotto['nazione']] += prodotto['punteggi']
    ordinate = sorted(nazioni.items(), key=ordina_d, reverse=True) #ordinare dizionario di dizionari
    return ordinate

def leggi_categorie(products):
    categorie = {}
    lista = []
    for p in products:
        if p['categoria'] not in categorie:
            categorie[p['categoria']] = [(p['punteggi'], p['codice'])]
        else:
            categorie[p['categoria']].append((p['punteggi'], p['codice']))
    for i in categorie:
        massimo = max(categorie[i])
        lista.append([i, massimo])
        # lista.append(massimo)
    return lista




def main():
    lista_prodotti, vincitore = leggi_file('prodotti.txt')
    nazioni = classifica_nazzioni(lista_prodotti)
    categorie = leggi_categorie(lista_prodotti)
    print(categorie)
    print(f" il miglior punteggio è: {vincitore['codice']} {vincitore['categoria']} con punteggio {vincitore['punteggi']}")
    print('classifica nazioni:')
    for i in range(3):
        print(f" {i+1}) {nazioni[i][0]} con punteggi {nazioni[i][1]}")
    print(f"classifiche categorie:")
    for element in categorie:
        print(f"[DEBUG] {element} -- {element[0]} prod. {element[1][1]} {element[1][0]} punti")


if __name__ == "__main__":
    main()