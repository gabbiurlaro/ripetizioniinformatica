# Manutenzione Macchina in Officina
Il file “manutenzione.txt” contiene la lista delle manutenzioni effettuate o da effettuare sulla propria autovettura e la data in cui vanno effettuate o sono state effettuate.
Ogni riga del file corrisponde ad una operazione, ed è composta dal nome (senza spazi) e dalla (gg/mm/aaaa) data in cui l’operazione è stata effettuata o da effettuare e il costo della manutenzione. 


Si scriva un programma che riceva 3 input: 

1. il nome del file, 
2. una data nel formato gg/mm/aaaa e 
3. un parametro “a” o “p”. 

Il programma dovrà stampare a schermo le operazioni effettuate prima della data inserita se il parametro "a” e quelle da effettuare in futuro se il parametro è “p”.
Ed in entrambi i casi stampare la manutenzione più cara e più economica nel perido in analisi



# Esempio di file "manutenzione.txt":
```
Controllo_Testata 23/7/2017 200
Cambio_Luci 15/8/2018 20
Lucidatura 25/8/2018 50
Lavaggio 12/9/2019 20 
Controllo_Cilindro 17/9/2019 150
```

# Messaggi stampati a video dal programma:

Esempio 1:

file: “manutenzione.txt” data: 26/08/2018 paramtero: a
```
Le operazioni effettuate prima del 26/08/2018 sono:
Controllo_Testata 23/7/2017 200
Cambio_Luci 15/8/2018 20
Lucidatura 25/8/2018 50

La manutenzione più costosa è stata Controllo_Testata del 23/7/2017 costata 200 euro
La manutenzione più economica è stata Cambio_Luci del 15/8/2018 costata 20 euro
```
Esempio 2:

file: “manutenzione.txt” data: 26/08/2018 paramtero: p
```
Le operazioni da effettuare dopo il 26/08/2018 sono:
Lavaggio 12/9/2019 20
Controllo_Cilindro 17/9/2019 150 

La manutenzione più costosa da effetuare è Controllo_Cilindro in data 23/7/2017 al costo di 150
La manutenzione più economica da effetuare è Lavaggio in data 12/9/2019 al costo di 20
```
