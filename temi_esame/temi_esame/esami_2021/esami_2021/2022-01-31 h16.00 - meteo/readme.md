# Esame "Meteo"

Si considerino il dati meteorologici della città di Torino nell'anno 2021, scaricati in formato CSV dal
sito www.ilmeteo.it. Esistono **12 file CSV**, uno per ogni mese dell'anno, il cui nome è "Torino-2021-Gennaio.csv" fino
a "Torino-2021-Dicembre.csv".

NOTA: i file CSV hanno i campi separati da un punto-e-virgola ';' anziché da una semplice virgola. Chi volesse
utilizzare le funzioni del modulo "csv" deve usare l'argomento **encoding=';'**.

Tutti i file CSV hanno lo stesso formato e contengono i seguenti campi (colonne):

- LOCALITA: stringa, nel nostro caso sempre pari a "Torino"
- DATA: giorno della misurazione, nel formato g/m/aaaa (esempio: 1/1/2021 o 25/12/2021)
- TMEDIA °C: numero reale
- TMIN °C: numero reale
- TMAX °C: numero reale
- PUNTORUGIADA °C: numero reale
- UMIDITA %: numero reale
- VISIBILITA km: numero reale
- VENTOMEDIA km/h: numero reale
- VENTOMAX km/h: numero reale
- RAFFICA km/h: numero reale
- PRESSIONESLM mb: numero reale
- PRESSIONEMEDIA mb: numero reale
- PIOGGIA mm: numero reale
- FENOMENI: contiene 0, 1 o più stringhe che descrivono i fenomeni verificatisi nella giornata, separati da uno spazio.
  Esempio: "pioggia nebbia ".


Scrivere un programma Python che offra le seguenti funzionalità:

1. leggere tutti i dati presenti in tutti i 12 file
2. determinare quali sono tutti i possibili FENOMENI che si sono verificati nel corso dell'anno e visualizzarli
3. permettere all'utente di inserire (da tastiera) uno dei fenomeni
4. determinare le date di tutti i giorni in cui si è verificato il fenomeno, stampando per ciascuno di essi: data, temperatura media, umidità e mm di pioggia
5. calcolare e stampare la temperatura minima, media e massima calcolate sull'insieme di tutti i giorni in cui si è verificato il fenomeno scelto dall'utente.
6. ripetere dal punto 3, o interrompere l'esecuzione se l'utente inserisce "fine".

Si noti che, per il calcolo della temperatura media complessiva, è necessario fare la media tra i diversi dati TMEDIA °C.


## Esempio del formato dei file
```
	LOCALITA;DATA;TMEDIA °C;TMIN °C;TMAX °C;PUNTORUGIADA °C;UMIDITA %;VISIBILITA km;VENTOMEDIA km/h;VENTOMAX km/h;RAFFICA km/h;PRESSIONESLM mb;PRESSIONEMEDIA mb;PIOGGIA mm;FENOMENI
	Torino;1/11/2021;"9";"6";"11";"9";"99";"5";"4";"9";"0";"1006";"0";"0";"pioggia nebbia "
	Torino;2/11/2021;"8";"2";"15";"5";"82";"21";"6";"11";"0";"1002";"0";"0";"nebbia "
	Torino;3/11/2021;"9";"9";"11";"8";"92";"12";"8";"15";"0";"1002";"0";"0";"pioggia "
	Torino;4/11/2021;"9";"5";"15";"6";"83";"13";"6";"13";"0";"1005";"0";"0";""
```

## Esempio di output

```
    Fenomeni esistenti:  nebbia, neve, pioggia, temporale
    Scegli: neve
    Data: 1/1/2021, Temperatura: 1.0, Umidità: 92.0, Pioggia: 0.0
    Data: 3/1/2021, Temperatura: 4.0, Umidità: 93.0, Pioggia: 0.0
    Data: 12/2/2021, Temperatura: 3.0, Umidità: 82.0, Pioggia: 0.0
    Data: 13/2/2021, Temperatura: -1.0, Umidità: 77.0, Pioggia: 0.0
    Data: 8/12/2021, Temperatura: 1.0, Umidità: 93.0, Pioggia: 0.0
    Temperature: media=1.6, minima=-3.0, massima=6.0
    Scegli: fine
```
