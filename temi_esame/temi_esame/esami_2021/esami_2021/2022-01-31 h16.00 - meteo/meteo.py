FILENAMES = [
    "Torino-2021-Agosto.csv",
    "Torino-2021-Aprile.csv",
    "Torino-2021-Dicembre.csv",
    "Torino-2021-Febbraio.csv",
    "Torino-2021-Gennaio.csv",
    "Torino-2021-Giugno.csv",
    "Torino-2021-Luglio.csv",
    "Torino-2021-Maggio.csv",
    "Torino-2021-Marzo.csv",
    "Torino-2021-Novembre.csv",
    "Torino-2021-Ottobre.csv",
    "Torino-2021-Settembre.csv"
]

MESI = [
    "Gennaio",
    "Febbraio",
    "Marzo"
]

def leggi_file(filenames):
    giornate = list()
    fenomeni = set()
    for mese in MESI:
        filename = f"Torino-2021-{mese}.csv"
        try:
            with open(filename, "r", encoding="utf-8") as file:
                file.readline()
                for line in file:
                    fields = line.rstrip().split(";")
                    data = fields[1]
                    t_min = float(fields[3].strip('"'))
                    t_media = float(fields[2].strip('"'))
                    t_max = float(fields[4].strip('"'))
                    h = float(fields[6].strip('"'))
                    pioggia = float(fields[-2].strip('"'))
                    f = fields[-1].split(" ")

                    for i in range(len(f)):
                        f[i] = f[i].replace('"', '')

                    giornate.append({
                        'data': data,
                        't_min': t_min,
                        't_max': t_max,
                        't_media': t_media,
                        'umidità': h,
                        'pioggia': pioggia,
                        'fenomeni': f
                    })

                    for x in f:
                        if x != '':
                            fenomeni.add(x)

        except FileNotFoundError:
            print(f"{filename} not found")
            exit(-1)
    return giornate, fenomeni

def scegli_giornate(giornate, x):
    result = []
    max_t = 0
    min_t = 0
    media_t = 0
    for giornata in giornate:
        if x in giornata['fenomeni']:
            result.append(giornata)
            t_1, t_2, t_3 = giornata['t_min'], giornata['t_max'], giornata['t_media']
            if t_1 > max_t:
                max_t = t_1
            elif t_1 < min_t:
                min_t = t_1

            if t_2 > max_t:
                max_t = t_2
            elif t_2 < min_t:
                min_t = t_2

            media_t += t_3

    return result, (min_t, max_t, media_t/len(result))


def main():

    giornate, fenomeni = leggi_file(FILENAMES)
    print(fenomeni)

    x = input("Scegli: ")
    while x != 'fine':
        if x not in fenomeni:
            print("Fenomeno non valido. RIprova")
        else:
            result, temperature = scegli_giornate(giornate, x)
            for element in result:
                print(element)
            print(temperature)
        x = input("Scegli: ")

if __name__ == "__main__":
    main()