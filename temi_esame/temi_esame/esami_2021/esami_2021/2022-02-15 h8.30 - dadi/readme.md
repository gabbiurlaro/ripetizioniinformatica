# PlayMatch: partita a dadi

Scrivere un programma che simula delle partite a un gioco con dadi. Il programma è strutturato in due funzioni principali, che vengono dettagliate di seguito. 

Il programma deve contenere una prima funzione, **playMatch**, che simula una singola partita con le seguenti regole: 

- In ogni lancio, i dadi usati sono due. 
- **Al primo lancio**: 
	- Se la somma dei dadi è 7 o 11, la partita è ***vinta***. Ad esempio, se i dadi valgono 5 e 2 (somma 7), la partita è vinta. 
	- Se la somma è 2, 3 oppure 12 la partita è ***persa***. Ad esempio, se i dadi valgono 2 ed 1 (somma 3) la partita è persa. 
	- Se la somma è diversa da questi valori (e.g. i due dadi sono 4 e 4, con somma 8), allora la somma diventa il ***nuovo punteggio vincente*** da ottenere ai prossimi lanci.  
- **Ai lanci successivi** valgono queste **<u>nuove regole</u>**: 
	- Se la somma dei due dadi è **7**, e la partita è ***persa***.
	- Se la somma è uguale al **punteggio vincente da ottenere** (e.g., 8 nell'esempio precedente), la partita è ***vinta***. 
	- Altrimenti continuo a lanciare dadi. 

La funzione deve ritornare la lista dei lanci effettuati e se la partita è stata vinta. 


## Esempi di partite

```
Partita 1, primo lancio 3 e 4 - partita vinta perchè il punteggio è 7
	
Partita 2, primo lancio 1 e 2 - partita persa perchè il punteggio è 3
	
Partita 3, primo lancio: 6 e 4 - la partita prosegue con 7 come punteggio perdente e 10 (6+4) come punteggio vincente (6+4)
	Secondo lancio: 3 e 2 - la somma è 5 ed è diversa da 7 e da 10, quindi continuo a lanciare
	Terzo lancio: 5 e 5, la partita è vinta perché ho ottenuto la somma 10
```

Il programma contiene poi una funzione **playMatches** che prende come parametro il numero di partite che devono essere giocate (n). La funzione playMatches invoca la funzione playMatch n volte, per simulare n partite. Per ogni partita deve inoltre scrivere una riga su un file "matches.txt", dove ogni riga contiene:  
```
<ID partita> <lancio1> <lancio2> … <lancioN>
```
dove: 

- ID partita è l’identificativo, dato da ‘ID’ seguito da un numero intero crescente
- lancio1 ... lancioN sono i risultati dei lanci dei dati
- I campi sono divisi dallo spazio

La funzione playMatches deve inoltre stampare a video: 

- quante partite sono state vinte, come valore assoluto e come percentuale sul totale di partite; 
- quante partite sono state vinte per un certo numero di lanci (in ordine crescente di numero di lanci).
  

La funzione **main** dovrà invocare quindi la funzione playMatches, passando il numero di partite da giocare (ad esempio 10). 

## Esempio di esecuzione con n = 10:
Di seguito vengono riportati il file **matches.txt** generato e il corrispondente output

## Esempio di contenuto di matches.txt
```
ID0 2 5 
ID1 2 2 1 6 
ID2 2 1 
ID3 2 2 4 4 2 6 5 2 
ID4 4 4 6 4 5 2 
ID5 5 1 6 2 2 4 
ID6 5 2 
ID7 1 5 2 1 1 3 4 5 1 5 
ID8 4 1 2 5 
ID9 1 1	
```

# Esempio di output:
```
Vinte 4 partite su 10 (40.00%)
2  lanci:  4  partite
4  lanci:  2  partite
6  lanci:  2  partite
8  lanci:  1  partite
10 lanci:  1  partite
```
