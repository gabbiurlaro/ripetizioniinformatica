"""
Scrivere un programma che chieda all'utente di inserire le dimensioni
di una tabella(matrice) e richieda input i numeri, uno per volta, assumendo
che non commetta errori. Stampare poi a video la matrice in questo modo:

1 2 3
4 5 6
7 8 9

"""

tabella = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8]
]

print(f"Tabella: {tabella}")

# for-each
for row in tabella:
    for element in row:
        print(f"{element:3d} ", end="")
    print("")

# for-loop
for i in range(len(tabella)):
    for j in range(len(tabella[i])):
        print(f"{tabella[i][j]:3d}", end="")
    print("")