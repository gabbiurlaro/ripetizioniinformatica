"""
Scrivere un programma che aiuti un professore a tenere traccia dei voti dell'esame di Analisi 1.
"""

# somma di ogni riga
# la somma di ogni riga è > 18?
# Calcola la media di questa sessione
# Calcola la media solo di chi ha passato l'esame

"""
1. Leggere il file e fare la somma(mettere la somma in una lista)
2. Fare le analisi
"""


def leggi_file(filename):
    lista_voti = []
    try:
        file = open(filename, "r", encoding="utf-8")
    except FileNotFoundError:
        print(f"WARNING: File not found!")
        exit(-1)  # fa terminare il programma con un codice
    # Significa che è andato tutto bene e possiamo continuare a leggere il file
    for line in file.readlines():
        somma = 0
        for element in line.split(" "):
            somma += int(element)
        lista_voti.append(somma)
    file.close()
    return lista_voti


def main():
    voti = leggi_file("voti.txt")
    print(f"Lista dei voti: {voti}")

    # Quante persone hanno superato l'esame ?
    superato_esame = 0
    for i in range(len(voti)):
        if voti[i] >= 18:
            superato_esame += 1

    # Calcola la media di questa sessione
    media_t = 0
    somma_t = 0
    for i in range(len(voti)):
        somma_t += voti[i]
    media_t = somma_t / len(voti)

    # Calcola la media di chi ha passato l'esame
    media = 0
    somma = 0
    for i in range(len(voti)):
        if voti[i] >= 18:
            somma += voti[i]
    media = somma / superato_esame

    print(f"Hanno superato l'esame {superato_esame} persone")
    print(f"La media complessiva è: {media_t}")
    print(f"La media di chi ha passato l'esame è {media}")

if __name__ == "__main__":
    main()
