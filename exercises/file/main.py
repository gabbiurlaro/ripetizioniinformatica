def main():
    flag = True
    while flag:
        filename = input("Inserisci il nome del file: ")
        try:
            file = open(filename, "r", encoding="utf-8")
            flag = False
        except FileNotFoundError:
            print(f"{filename} non trovato. Prova di nuovo.")


if __name__ == "__main__":
    main()